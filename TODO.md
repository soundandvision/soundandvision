* [X] Hardware/Setup Overview
* [X] Data Review
* [ ] Firmware
  * [X] Firmware build completed, initial
    * raptor-op-build doesn't build on disco - use podman container at bionic lts
  * [ ] Simulation setup for Talos 2 Cluster
  * [ ] Firmware build completed, automated
* [ ] Fedora CoreOS
  * [X] FCOS Assembler Build, initial
    * Assembler Docker image qemu based on Fedora 31 doesn't boot - downgraded to Fedora 30, boots
  * [X] FCOS Initial image built
* [ ] Bootstrap K8s
  * [ ] Bootstrap K8s on Local Containers (KIND - kubernetes in docker)
    * [X] Initial KIND build (local)
      * uses reproducible build architecture in kind repo
    * [X] KIND Containers (base / node) built
      * Multiple source level hacks for build on podman (in both k8s and kind repos)
        * [ ] Upstream patches and patchsets
    * [ ] Launch KIND cluster on local podman container set
      * [ ] Resolve /sys RW mount issue (currently podman `--privileged` mounts as read-only)
  * [ ] Bootstrap K8s on swarm (check to make sure swarm is still in FCOS)
* [ ] Storage on K8s
* [ ] PostgreSQL on K8s
* [ ] JupyterLab setup on K8s

